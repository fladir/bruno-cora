<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'brunocora' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'U.EFJX#;Jv#cfjA2D}G[$q<( -XJp>n9*ZLJT-!41!@x&)1*5+50rax0b~Dl3JQp' );
define( 'SECURE_AUTH_KEY',  '08+OH})/<Xtw0v;7WlewS[^re^+z5EC:Je74X.s$M(H!s/#.$+UtC&-K.u#zqws*' );
define( 'LOGGED_IN_KEY',    '2*CBX9>M&WuG$<1}&0uKWze7;yW`nv,KXvz;[: C(smHJ]NY^m@YyI@1d]BF4nm;' );
define( 'NONCE_KEY',        'c]G*2h<NAbb]7t!IU<$NWawl9dq,]5K9?yqMjD-q182YBNQ5]t~<@$HS1 lGlke=' );
define( 'AUTH_SALT',        '>Sk!oBsI)j4f&Wjv4Mg[H^!LW[i^4J8#%UADr$MQjxG*02:5DG(k8-gmzj)Qh,~L' );
define( 'SECURE_AUTH_SALT', 'oEF-j[exF(Ad?nO#U<LL8z|+0mD- c,t`23zK~B}~YU1iM=#RwXT_8H_[gTg]dVe' );
define( 'LOGGED_IN_SALT',   '#&k S<6b2<Vu]}UF_O>80HzZ3*VgYL{ZgG/t,mfgbLh7=>.UYT!}j,*YZ4AF,afA' );
define( 'NONCE_SALT',       '(qK</Vaw|O- 9I1;9) :H[utwR|_B;dKK^*ZBO9c07r4y~gGb8WE(hqge^*i%A/d' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
