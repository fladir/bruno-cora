<?php get_header() ?>

    <!-- Banner -->
<?php get_template_part('components/banner/banner'); ?>

    <!-- Seções Alternadas -->
<?php get_template_part('components/secoes-alternadas/secoes-alternadas'); ?>

    <!-- Chamada 1 -->
<?php get_template_part('components/chamadas/chamada-1'); ?>

    <!-- Profissionais -->
<?php get_template_part('components/profissionais/profissionais'); ?>

    <!-- Como Funciona -->
<?php get_template_part('components/como-funciona/como-funciona'); ?>

    <!-- Resultados -->
<?php get_template_part('components/resultados/resultados'); ?>

    <!-- CTA Download App -->
<?php get_template_part('components/cta-download-app/cta-download-app'); ?>

    <!-- Seções Alternadas 2 -->
<?php get_template_part('components/secoes-alternadas/secoes-alternadas-2'); ?>

    <!-- Boxes Consultoria -->
<?php get_template_part('components/boxes-consultoria/boxes-consultoria'); ?>

    <!-- FAQ -->
<?php get_template_part('components/faq/faq'); ?>

    <!-- Chamada Whatsapp -->
<?php get_template_part('components/chamada-whatsapp/chamada-whatsapp'); ?>

    <!-- Quem Sou Eu -->
<?php get_template_part('components/quem-sou-eu/quem-sou-eu'); ?>

    <!-- Depoimentos -->
<?php get_template_part('components/depoimentos/depoimentos'); ?>

    <!-- Chamada 2 -->
<?php get_template_part('components/chamadas/chamada-2'); ?>

    <!-- Formas de Pagamento -->
<?php get_template_part('components/formas-de-pagamento/formas-de-pagamento'); ?>


<?php get_footer() ?>