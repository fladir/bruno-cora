(function ($) {

    $(document).ready(function () {


        $('a[href^="#"]').on('click', function (e) {
            e.preventDefault();
            $(document).off("scroll");

            $('.nav-link').each(function () {
                $(this).removeClass('active');
            });
            $(this).addClass('active');

            var target = this.hash,
                menu = target;
            $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 1000, 'easeInOutExpo', function () {
                window.location.hash = target;
                $(document).on("scroll", onScroll);
            });
        });




        $('[data-fancybox]').fancybox({
            autoStart: true,
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#voltarTopo').addClass('show')
            } else {
                $('#voltarTopo').removeClass('show');
            }
        });
        $('#voltarTopo').click(function () {
            $("html, body").animate({scrollTop: 0}, 1200, 'easeInOutExpo');
            return false;
        });

        // Carousel Depoimentos
        $(".depoimentos-carousel").owlCarousel({
            loop: false,
            items: 3,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: false,
            autoplaySpeed: 1000,
            autoplayHoverPause: true,
            margin: 50,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                }
            }
        });

        // Resultados Portfolio
        $(".resultados-carousel").owlCarousel({
            loop: true,
            items: 3,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: false,
            autoplaySpeed: 1000,
            autoplayHoverPause: true,
            margin: 60,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                }
            }
        });
        // Carousel Frases Quem Su Eu
        $(".frases-carousel").owlCarousel({
            loop: true,
            items: 1,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 500,
            autoplayHoverPause: true,
        });

        // Carousel Depoimentos
        $(".depoimento-carousel").owlCarousel({
            loop: true,
            items: 1,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 500,
            slideBy: 1,
            margin: 25,
            autoplayHoverPause: true
        });
        $(".slider-home-carousel").owlCarousel({
            loop: false,
            items: 1,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 500,
            slideBy: 1,
            margin: 0,
            autoplayHoverPause: true
        });

        //WOW Effect
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        );
        wow.init();
        inView('.card-item').on('enter', function (el) {
            jQuery(this).addClass('active');
        });

        document.querySelector('.card-item').addEventListener(
            'scroll',
            function () {
                var scrollTop = document.querySelector('.card-item').scrollTop;
                var scrollHeight = document.querySelector('.card-item').scrollHeight; // added
                var offsetHeight = document.querySelector('.card-item').offsetHeight;
                // var clientHeight = document.getElementById('box').clientHeight;
                var contentHeight = scrollHeight - offsetHeight; // added
                if (contentHeight <= scrollTop) // modified
                {
                    console.log('teste')
                }
            },
            false
        );

        $(".fancybox").fancybox();

    });

    //Botão Menu Mobile
    $('.nav-toggler, .mobile-menu-overlay').click(function () {
        $('.nav-toggler').toggleClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
    });
    $('.nav-link').click(function () {
        $('.nav-toggler').removeClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').removeClass('open');
    });

    //Contagem Nossos Números
    $('.nav-link').removeAttr('title');


    //Waypoint Section Como Funciona
    $(document).ready(function () {
        var ppWaypoint = $('.card-item').waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('show');
            } else {
                $(this.element).removeClass('show');
            }
        }, {offset: '25%'});
    });


})(jQuery);
