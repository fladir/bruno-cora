<?php
$grupoFooter = get_fields('options')['grupo_footer'];
$grupoCertificados = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_certificados'];
$certificacoes = $grupoCertificados['certificados'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];
$menuLateral = get_field('grupo_informacoes_para_contato', 'options')['menu_lateral'];
?>

<!-- Footer -->
<footer class="font-small text-white">
    <div class="container text-left wrapper-footer">
        <div class="row termos">
            <div class="col-12 termos-e-condicoes">
                <h4>TERMOS E CONDIÇÕES</h4>
                <?php echo $grupoFooter['termos_e_condicoes']; ?>
            </div>
        </div>

        <div class="row info">
            <div class="col-md-4 sobre">
                <a class="logo-footer mb-3" href="<?php bloginfo('url'); ?>">
                    <?php echo wp_get_attachment_image(get_field('grupo_footer', 'options')['logo_footer'], 'footer_logo'); ?>
                    <p class="texto-footer">
                        <?php echo $grupoFooter['Texto_footer'] ?>
                    </p>

                </a>
            </div>
            <div class="col-md-4 menu">
                <h3>Navegue</h3>
                <?php

                wp_nav_menu(array(
                    'theme_location' => 'secondary',
                    'depth' => 1,
                    'container' => 'div',
                    'container_class' => 'navbar-collapse justify-content-end site-navbar',
                    'container_id' => 'navbarNav',
                    'menu_class' => 'nav navbar-nav navbar-desktop',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker(),
                ));

                ?>
            </div>

            <div class="col-md-4 contato">
                <h3 class="mb-3">Contato</h3>
                <?php foreach ($emails as $email) : ?>
                    <span class="email mr-3">
                                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                                    <?php echo $email['endereco_email']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>

                <?php foreach ($telefones as $telefone) : ?>
                    <span class="telefone mr-3">
                                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                                    <?php echo $telefone['numero_telefone']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>
                <div class="redes-sociais">
                    <?php foreach ($redesSociais as $redesSocial) : ?>
                        <span class="rede-social mr-2">
                                    <a href="<?php echo $redesSocial['link_social']; ?>" target="_blank"
                                       title="<?php echo $redesSocial['nome_rede_social']; ?>">
                                    <i class="<?php echo $redesSocial['icone_social']; ?>"></i>
                                    </a>
                                </span>
                    <?php endforeach; ?>
                </div>
                <a href="#" class="link-pv" title="Contato" rel="nofollow noreferrer noopener external"
                   data-toggle="modal"
                   data-target="#politicaDePrivacidade">
                    Política de Privacidade
                </a>
            </div>

        </div>


    </div>

    <!-- Copyright -->
    <?php get_template_part('/components/footer/cia-logo-footer'); ?>
    <!-- Copyright -->

</footer>
<div class="mobile-menu-overlay"></div>
<div class="botao-lateral-wrapper">
    <div class="botao-lateral">
        <!-- Menu Lateral -->
        <?php foreach ($menuLateral as $item) : ?>
            <a href="<?php echo $item['link_social']; ?>" target="_blank"
               title="<?php echo $item['nome_do_botao']; ?>">
                <i class="<?php echo $item['icone_social']; ?>"></i>
            </a>
        <?php endforeach; ?>
    </div>
</div>
<!-- Footer -->
<div class="modal fade" id="politicaDePrivacidade" tabindex="-1" role="dialog" aria-labelledby="politicaDePrivacidade"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title fw-bold text-center w-100">Política de Privacidade</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="wrapper-form-modal p-5">
                <?php echo $grupoFooter = get_fields('options')['grupo_footer']['politica_de_privacidade']; ?>
            </div>
        </div>
    </div>
</div>

<?php wp_footer() ?>

</body>

</html>