<?php
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
    add_image_size('logo_header', 392, 100); //Logo Header
    add_image_size('banner', 1920, 850); // Imagem Banner
    add_image_size('imagens_col6', 960, 590, true); // Imagens metade da página
    add_image_size('diferenciais', 162, 145); // Ícones Diferenciais
    add_image_size('img_chamada_1', 492, 535); // Imagem Chamada 1
    add_image_size('secoes_alternadas', 800, 800); // Seções Alternadas
    add_image_size('footer_logo', 374, 68); // Footer Logo
    add_image_size('como_funciona', 672, 880, true); // Como Funciona
    add_image_size('portfolio_big', 800, 600, true); // Portfólio Big
    add_image_size('depoimentos', 150, 150, true); // Depoimentos
    add_image_size('profissionais', 328, 302); // Profissionais
    add_image_size('resultados_small', 328, 390); // Resultados Small
    add_image_size('resultados_big', 328, 390); // Resultados Small
    add_image_size('download_app', 536, 480); // Download App
    add_image_size('ic_box_consultoria', 120, 120); // Icones Boxes Consultoria
    add_image_size('chamada_whatsapp', 563, 522); // Chamada Whatsapp
    add_image_size('quem_sou_eu', 508, 890); // Quem Sou Eu
    add_image_size('formas_de_pagamento', 70, 35); // Formas de Pagamento
}