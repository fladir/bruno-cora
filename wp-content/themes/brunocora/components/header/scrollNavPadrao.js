(function ($) {
    $(document).ready(function () {
        $(window).resize(function () {
            var lastScrollTop = 0;
            
                $(window).scroll(function (event) {
                    var st = $(this).scrollTop();
                    if (st > lastScrollTop && st > 40) {
                        $('#menu-secundario').addClass('animated fixed-top menu_sticky');
                        $('.nav-toggler').addClass('sticky');
                    } else if (st < 40) {
                        $('#menu-secundario').removeClass('fixed-top menu_sticky')
                        $('.nav-toggler').removeClass('sticky');
                    } else {
                        $('#menu-secundario').addClass('fadeIn fixed-top menu_sticky');
                        $('.nav-toggler').addClass('sticky');
                    }
                    lastScrollTop = st;
                });

            
        }).resize();


    });



    $(document).ready(function () {
        $(document).on("scroll", onScroll);
        $('.nav-link[href^="#menuPrimario"]').addClass('active');
        //smoothscroll
        $('.nav-link[href^="#"]').on('click', function (e) {
            e.preventDefault();
            $(document).off("scroll");

            $('.nav-link').each(function () {
                $(this).removeClass('active');
            });
            $(this).addClass('active');

            var target = this.hash,
                menu = target;
            $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 1000, 'easeInOutExpo', function () {
                window.location.hash = target;
                $(document).on("scroll", onScroll);
            });
        });
    });

    function onScroll(event){
        var scrollPos = $(document).scrollTop();
        $('.nav-link').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('.nav-link ul li a').removeClass("active");
                currLink.addClass("active");
            }
            else{
                currLink.removeClass("active");
            }
        });
    }


})(jQuery);