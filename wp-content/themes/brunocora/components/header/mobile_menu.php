<div class="mobile-menu-wrapper">
    <?php
    wp_nav_menu(array(
        'theme_location' => 'primary',
        'depth' => 3,
        'container' => 'div',
        'container_class' => 'navbar-collapse justify-content-end site-navbar',
        'container_id' => 'navbarNav',
        'menu_class' => 'nav navbar-nav mobile-menu',
        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
        'walker' => new WP_Bootstrap_Navwalker(),
    ));

    $botaoPrimario = get_fields('options')['grupo_header']['botao_primario'];
    $botaoSecundario = get_fields('options')['grupo_header']['botao_secundario'];
    $telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
    $emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
    $botaoWhatsapp = get_fields('options')['grupo_informacoes_para_contato']['whatsapp'];


    ?>
    <div class="contatos-mobile-wrapper">
    <?php foreach ($telefones as $telefone) : ?>
        <span class="telefone mb-2">
                    <i class="fas fa-phone-alt mr-2 "></i>
                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                    <?php echo $telefone['numero_telefone']; ?>
                    </a>
                </span>
    <?php endforeach; ?>
    <?php foreach ($emails as $email) : ?>
        <span class="email">
                    <i class="fas fa-envelope mr-2 "></i>
                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                    <?php echo $email['endereco_email']; ?>
                    </a>
                </span>
    <?php endforeach; ?>

    </div>
    <?php if ($botaoWhatsapp) : ?>
        <div class="button_su button_whatsapp w-100">
            <span class="su_button_circle"></span>
            <a style="font-size: 13px;" href="https://api.whatsapp.com/send?phone=55<?php echo $botaoWhatsapp[0]['link_whatsapp'] ?>&text=Ol%C3%A1,%20tudo%20bem?"
               class="btn button_su_inner btn-whatsapp-topo">
                <span class="button_text_container"><i class="fab fa-whatsapp"></i><?php echo $botaoWhatsapp[0]['numero_whatsapp'] ?></span>


            </a>
        </div>

    <?php endif; ?>


</div>



