<?php
$formasDePagamento = get_field('formas_de_pagamento')
?>
<?php if ($formasDePagamento) : ?>
    <section id="formas-de-pagamento">
        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4 px-3">
                    <h3>FORMAS DE PAGAMENTO</h3>
                    <div class="formas-de-pagamento d-flex justify-content-between align-items-center">
                        <?php foreach ($formasDePagamento as $item) : ?>
                            <img src="<?php echo $item['forma_de_pagamento']['sizes']['formas_de_pagamento'] ?>"
                                 alt="Forma de Pagamento">
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>