<?php
$args = array(
    'nopaging' => false,
    'post_type' => 'depoimento',
    'posts_per_page' => 36,
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);

?>

<section id="depoimentos" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>DEPOIMENTOS</h3>
                <div class="owl-carousel owl-theme depoimentos-carousel">
                    <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post();
                        ?>
                        <div class="item">

                            <a href="<?php echo get_field('link_do_video_depoimento') ?>"
                               class="d-flex link-video-depoimento justify-content-center flex-column fancybox"
                               data-toggle="lightbox" data-gallery="example-gallery"">

                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-white.png"
                                 class="logo-white-app"/>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/btn-youtube.png"
                                 class="btn-youtube"/>


                            </a>

                            <h4><?php the_title() ?> (<?php echo get_field('local_do_cliente') ?>)</h4>
                            <?php the_content() ?>

                        </div>
                    <?php

                    endwhile;
                    endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
