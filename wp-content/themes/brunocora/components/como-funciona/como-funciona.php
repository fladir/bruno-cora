<?php
$comoFunciona = get_field('como_funciona');
$btnComoFunciona = get_field('botao_como_funciona');
?>
<section id="como-funciona" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3><?php echo get_field('titulo_como_funciona') ?></h3>
                <img src="<?php echo get_field('imagem_como_funciona')['sizes']['como_funciona'] ?>"
                     alt="<?php echo get_field('titulo_como_funciona') ?>">
            </div>
            <div class="col-md-6">
                <div class="cards">
                    <?php $i = 0;
                    if ($comoFunciona) : foreach ($comoFunciona as $item) : ?>
                        <div class="card-item">
                            <div class="card-inner">
                                <div class="card-text">
                                    <?php echo $item['texto'] ?>
                                </div>
                                <div class="card-arrow"></div>
                            </div>
                            <div class="card-point">
                                <div class="card-point-content">
                                    <?php echo $i + 1; ?>
                                </div>
                            </div>
                        </div>
                        <?php $i++; endforeach; endif; ?>
                </div>
                <?php if ($btnComoFunciona['texto_do_botao']) : ?>
                    <a href="<?php echo $btnComoFunciona['link_do_botao'] ?>" class="btn-skewed-sm"
                       style="margin-left: 100px;">
                        <span><?php echo $btnComoFunciona['texto_do_botao'] ?></span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>