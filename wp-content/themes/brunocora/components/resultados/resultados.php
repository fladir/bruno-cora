<?php
$slideResultados = get_field('slide_resultados');
#echo '<pre>'; print_r($slideResultados); echo '</pre>';
?>

<section id="resultados" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-4">
                <h3><?php echo get_field('titulo_resultados') ?></h3>
            </div>
            <?php if ($slideResultados) : ?>
                <div class="col-12">
                    <div class="owl-carousel owl-theme resultados-carousel">
                        <?php foreach ($slideResultados as $item) : ?>
                            <div class="item">

                                <a class="fancybox" href="<?php echo $item['sizes']['resultados_big'] ?>" rel="gallery1"
                                   data-toggle="lightbox" data-gallery="example-gallery">
                                    <img src="<?php echo $item['sizes']['resultados_small'] ?>"
                                         class="lateral-profissionais"/>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-12 d-flex justify-content-center mt-2">
                    <?php if (get_field('texto_do_botao_resultados')) : ?>
                        <a href="<?php echo get_field('link_do_botao_resultados') ?>" class="btn-skewed-sm">
                            <span><?php echo get_field('texto_do_botao_resultados') ?></span>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>