<?php
$slides = get_field('slides');
//echo '<pre>'; print_r($slides); echo '</pre>';
?>
<section id="banner">
    <img src="<?php bloginfo('template_directory'); ?>/assets/img/particle-1.png" class="particles particle-1-top"/>
    <img src="<?php bloginfo('template_directory'); ?>/assets/img/particle-2.png" class="particles particle-2-top"/>

        <div class="owl-carousel owl-theme slider-home-carousel">
            <?php foreach ($slides as $slide) : ?>
                <div class="item">
                    <img src="<?php echo $slide['imagem']['sizes']['banner'] ?>"
                         alt="" class="attachment-banner">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2><?php echo $slide['titulo'] ?></h2>
                                <?php if($slide['texto_do_botao']) : ?>
                                <a href="<?php echo $slide['link_do_botao'] ?>" class="btn-banner">
                                    <span><?php echo $slide['texto_do_botao'] ?></span>
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/element-btn-banner.png" class="element-btn"/>
                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

</section>