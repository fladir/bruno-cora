<?php
$secoesAlternadas = get_field('secoes_alternadas_2');
#echo '<pre>'; print_r($secoesAlternadas); echo '</pre>';
?>

<section class="secoes-alternadas sa-2" id="e-book">
    <div class="container">
        <?php
        if (have_rows('secoes_alternadas')):
            $i = 0;
            while (have_rows('secoes_alternadas_2')) : the_row();
                $class = $i % 2 ? 'flex-row-reverse' : '';
                $img_side = $i % 2 ? 'right-img' : 'left-img';
                $after_bg = $i % 2 ? 'afbg-primario' : 'afbg-secundario';
                $second_img = $i == 1 ? 'second-img' : '';
                ?>
                <?php
                $imagem = get_sub_field('imagem');
                $titulo = get_sub_field('titulo');
                $subTitulo = get_sub_field('subtitulo');
                $texto = get_sub_field('texto');
                $textoDoBotao = get_sub_field('texto_do_botao');
                $linkDoBotao = get_sub_field('link_do_botao');
                ?>
                <div class="row <?php echo $class; ?> wow fadeInUp">
                    <div class="col-md-6">

                        <div class="wrapper-attachment-secoes-aternadas d-flex justify-content-center align-items-center ">
                            <img src="<?php echo $imagem['sizes']['secoes_alternadas'] ?>"
                                 alt="<?php echo $titulo ?>"
                                 class="attachment-secoes-aternadas <?php echo $img_side; ?> <?php echo $second_img; ?>">
                        </div>
                    </div>
                    <div class="col-md-6 sa-content sac-<?php echo $i; ?>">
                        <?php if ($titulo) : ?>
                            <h3 class="fw-bold"><?php echo $titulo ?></h3>
                        <?php endif; ?>
                        <?php if ($subTitulo) : ?>
                            <h4 class="text-uppercase"><?php echo $subTitulo ?></h4>
                        <?php endif; ?>
                        <?php echo $texto ?>
                        <?php if ($textoDoBotao) : ?>
                            <a href="<?php echo $linkDoBotao ?>" class="btn-skewed-sm">
                                <span><?php echo $textoDoBotao ?></span>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php
                $i++;
            endwhile;
        endif;
        ?>
    </div>
</section>

