<?php
$boxes = get_field('boxes');
//echo '<pre>';
//print_r($boxes);
//echo '</pre>';
?>
<?php if (have_rows('boxes')): ?>
    <section id="planos">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow fadeInUp">
                    <h3>
                        <?php echo get_field('titulo_da_secao_boxes') ?>
                    </h3>
                </div>
                <div class="col-md-6"></div>
                <?php while (have_rows('boxes')): the_row();
                    $textoBox = get_sub_field('titulo');
                    $iconeBox = get_sub_field('icone_boxes');

                    ?>
                    <div class="col-md wow fadeInUp mb-4 mb-md-0">
                        <div class="box-consultoria h-100">
                            <div class="header-and-items">
                                <div class="box-header">
                                    <h4>
                                        <span class="linha-1"><?php echo $textoBox['1_linha'] ?></span>
                                        <span class="linha-2"><?php echo $textoBox['2_linha'] ?></span>
                                        <span class="linha-3"><?php echo $textoBox['3_linha'] ?></span>
                                    </h4>
                                    <img src="<?php echo $iconeBox['sizes']['ic_box_consultoria'] ?>" alt="">
                                </div>
                                <div class="items-box">
                                    <?php
                                    $count = 0;
                                    while (have_rows('itens')): the_row(); ?>

                                        <p>
                            <span class="numero-item">
                                <span>
                                    <?php echo $count + 1; ?>
                                </span>
                            </span>
                                            <?php echo get_sub_field('texto_do_item') ?>
                                        </p>
                                        <?php $count++; endwhile; ?>
                                </div>
                            </div>
                            <?php if (get_sub_field('link_do_botao_boxes')) : ?>
                                <a href="<?php echo get_sub_field('link_do_botao_boxes') ?>"
                                   class="w-100 text-center btn-skewed-sm">
                                    <span><?php echo get_sub_field('texto_do_botao_boxes') ?></span>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>