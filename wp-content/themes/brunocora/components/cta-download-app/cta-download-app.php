<?php

?>


<section id="cta-download-app" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="text-uppercase">
                    <?php echo get_field('titulo_cta_download_app') ?>
                </h3>
                <div class="texto-cta-download-app">
                    <?php echo get_field('texto_download_app') ?>
                </div>
                <div class="botoes-download-app d-none d-md-block">
                    <?php if (get_field('botao_app_store')['link_do_botao']) : ?>
                        <a href="<?php echo get_field('botao_app_store')['link_do_botao'] ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/btn-app-store.svg"/>
                        </a>
                    <?php endif; ?>
                    <?php if (get_field('botao_google_play')['link_do_botao']) : ?>
                        <a href="<?php echo get_field('botao_google_play')['link_do_botao'] ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/btn-google-play.svg"/>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <img class="d-none d-md-block img-phone"
                     src="<?php echo get_field('imagem_download_app')['sizes']['download_app'] ?>"
                     alt="Imagem Download App">
            </div>
            <div class="col-6 d-block d-md-none">
                <div class="botoes-download-app">
                    <?php if (get_field('botao_app_store')['link_do_botao']) : ?>
                        <a href="<?php echo get_field('botao_app_store')['link_do_botao'] ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/btn-app-store.svg"/>
                        </a>
                    <?php endif; ?>
                    <?php if (get_field('botao_google_play')['link_do_botao']) : ?>
                        <a href="<?php echo get_field('botao_google_play')['link_do_botao'] ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/btn-google-play.svg"/>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-6 d-block pl-0 d-md-none">
                <img src="<?php echo get_field('imagem_download_app')['sizes']['download_app'] ?>"
                     alt="Imagem Download App">
            </div>
            <div class="col-10 offset-1">
                <a href="<?php echo get_field('video_download_app') ?>"
                   class="d-flex link-video-download-app justify-content-center flex-column fancybox"
                   data-toggle="lightbox" data-gallery="example-gallery"">

                <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-white.png" class="logo-white-app"/>
                <img src="<?php bloginfo('template_directory'); ?>/assets/img/btn-youtube.png" class="btn-youtube"/>


                </a>
            </div>
        </div>
    </div>
</section>
