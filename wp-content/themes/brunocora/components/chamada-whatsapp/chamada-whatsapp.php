<?php
$chamadaWhatsapp = get_field('chamada_whatsapp');
?>

<section id="suporte" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>
                    <?php echo $chamadaWhatsapp['titulo']; ?>
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <img src="<?php echo $chamadaWhatsapp['imagem']['sizes']['chamada_whatsapp'] ?>"
                     alt="<?php echo $chamadaWhatsapp['titulo']; ?>" class="img-chamada-whatsapp">
            </div>
            <div class="col-6 d-flex flex-column align-items-center justify-content-start justify-content-md-center">
                <img src="<?php bloginfo('template_directory'); ?>/assets/img/whatsapp-icon.svg" class="icon-whatsapp mb-3"/>
                <?php if ($chamadaWhatsapp['texto_do_botao']) : ?>
                    <a href="https://api.whatsapp.com/send?phone=<?php echo get_field('grupo_informacoes_para_contato', 'options')['whatsapp'][0]['link_whatsapp']; ?>&text=Ol%C3%A1%2C%20tudo%20bem%3F"
                       class="text-center btn-skewed-sm">
                        <span><?php echo $chamadaWhatsapp['texto_do_botao'] ?></span>
                    </a>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>

