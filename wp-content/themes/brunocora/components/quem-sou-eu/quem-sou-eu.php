<?php
$quemSouEu = get_field('quem_sou_eu');
$frases = get_field('frases');
?>

<section id="quem-sou-eu" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-6 content">
                <h3>
                    <?php echo $quemSouEu['titulo']; ?>
                </h3>
                <?php echo $quemSouEu['texto'] ?>
                <img src="<?php echo $quemSouEu['imagem']['sizes']['quem_sou_eu'] ?>" alt="Quem Sou Eu" class="img-quem-sou-eu-mobile d-block d-md-none">
                <?php if ($frases) : ?>
                    <div class="owl-carousel owl-theme frases-carousel">
                        <?php foreach ($frases as $frase) : ?>
                            <div class="item d-flex align-items-center justify-content-center">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/aspas-frases.png"
                                     class="aspa-frase-1"/>

                                <p>
                                    "<?php echo $frase['frase']; ?>"
                                </p>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/aspas-frases.png"
                                     class="aspa-frase-2"/>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <img src="<?php echo $quemSouEu['imagem']['sizes']['quem_sou_eu'] ?>" alt="Quem Sou Eu" class="img-quem-sou-eu d-none d-md-block">
            </div>
        </div>
    </div>
</section>
