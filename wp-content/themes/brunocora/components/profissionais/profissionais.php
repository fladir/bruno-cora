<?php
$args = array(
    'nopaging' => false,
    'post_type' => 'profissionais',
    'posts_per_page' => 36,
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);

$conteudoProtfolio = get_field('conteudo_portfolio');

?>

<section id="profissionais" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-theme profissionais-carousel">
                <?php

                if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post();
                    ?>
                    <div class="item">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/lateral-profissionais.png" class="lateral-profissionais"/>
                        <?php the_post_thumbnail('profissionais', array('class' => 'img-profissionais', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>

                        <div class="name-profession text-center">
                            <h4><?php the_title() ?></h4>
                            <h5>(<?php echo get_field('profissao') ?>)</h5>
                        </div>
                        <h6>
                            <?php echo get_field('n_de_registro') ?>
                        </h6>
                    </div>
                <?php

                endwhile;
                endif;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
