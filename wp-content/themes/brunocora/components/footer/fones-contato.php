<div class="row mt-5 mb-40 phones">
    <?php
    $telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
    foreach ($telefones as $telefone) : ?>
        <div class="col-6 col-md-3 col-lg-4 col-xl-3 my-3 my-md-1">
            <a href="">
            <?php echo $telefone['numero_telefone']; ?>
            </a>
        </div>
    <?php endforeach; ?>
</div>