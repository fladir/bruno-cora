<h5>Onde estamos</h5>
<?php $enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
foreach ($enderecos as $endereco) :
?>
    <div class="box-endereco">
        <span><?php echo $endereco['endereco']; ?></span>
        <span><?php echo $endereco['bairro']; ?></span>
        <span><?php echo $endereco['cidade_estado']; ?></span>
    </div>
<?php endforeach; ?>