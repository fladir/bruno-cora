<?php
$chamada2 = get_field('chamada_2');
?>

<section id="chamada-2" class="wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <?php if ($chamada2['texto_do_botao']) : ?>
                    <a href="<?php echo $chamada2['link_do_botao'] ?>"
                       class="text-center btn-skewed-sm">
                        <span><?php echo $chamada2['texto_do_botao'] ?></span>
                    </a>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>