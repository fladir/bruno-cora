<?php
$chamada = get_field('conteudo_da_chamada_1');
?>
<section id="atendimento" class="wow fadeInUp">
    <i class="fab fa-whatsapp"></i>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="<?php echo $chamada['imagem']['sizes']['img_chamada_1']; ?>">
            </div>
            <div class="col-md-6 d-flex flex-column justify-content-center content">
                <h3>
                    <?php echo $chamada['titulo'] ?>
                </h3>
                <p class="text-white">
                    <?php echo $chamada['texto'] ?>
                </p>
                <?php if ($chamada['texto_do_botao']) : ?>
                    <a href="https://api.whatsapp.com/send?phone=<?php echo get_field('grupo_informacoes_para_contato', 'options')['whatsapp'][0]['link_whatsapp']; ?>&text=Ol%C3%A1%2C%20tudo%20bem%3F"
                       class="btn-chamada-1">
                        <span><?php echo $chamada['texto_do_botao'] ?></span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>