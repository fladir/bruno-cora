msgid ""
msgstr ""
"Project-Id-Version: WhatsApp Chat by NinjaTeam\n"
"POT-Creation-Date: 2019-04-09 09:20+0700\n"
"PO-Revision-Date: 2019-04-09 09:20+0700\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: whatsapp.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: includes/nta-whatsapp-post-type.php:16
msgid "Contas de WhatsApp"
msgstr ""

#: includes/nta-whatsapp-post-type.php:17
msgid "Whatsapp Account"
msgstr ""

#: includes/nta-whatsapp-post-type.php:18
#: includes/nta-whatsapp-post-type.php:19
msgid "Adicionar nova conta"
msgstr ""

#: includes/nta-whatsapp-post-type.php:20
msgid "Edit Account"
msgstr ""

#: includes/nta-whatsapp-post-type.php:21
msgid "New Account"
msgstr ""

#: includes/nta-whatsapp-post-type.php:22
msgid "Todas as contas"
msgstr ""

#: includes/nta-whatsapp-post-type.php:23
msgid "View Accounts"
msgstr ""

#: includes/nta-whatsapp-post-type.php:24
msgid "Search Account"
msgstr ""

#: includes/nta-whatsapp-post-type.php:25
#: includes/nta-whatsapp-post-type.php:179
msgid "Avatar"
msgstr ""

#: includes/nta-whatsapp-post-type.php:26
msgid "Select an image"
msgstr ""

#: includes/nta-whatsapp-post-type.php:27
msgid "Remove avatar"
msgstr ""

#: includes/nta-whatsapp-post-type.php:32
msgid "Manager Accounts"
msgstr ""

#: includes/nta-whatsapp-post-type.php:178
msgid "Nome da Conta"
msgstr ""

#: includes/nta-whatsapp-post-type.php:180
#: includes/nta-whatsapp-post-type.php:191
msgid "Número"
msgstr ""

#: includes/nta-whatsapp-post-type.php:181
#: views/nta-whatsapp-meta-accounts.php:19
msgid "Título"
msgstr ""

#: includes/nta-whatsapp-post-type.php:182
msgid "Dias ativos"
msgstr ""

#: includes/nta-whatsapp-post-type.php:183
msgid "Shortcode"
msgstr ""

#: includes/nta-whatsapp-post-type.php:192
msgid "Time Slot"
msgstr ""

#: includes/nta-whatsapp-setting.php:37
msgid "Adicionar nova conta"
msgstr ""

#: includes/nta-whatsapp-setting.php:38
msgid "WhatsApp Flutuante"
msgstr ""

#: includes/nta-whatsapp-setting.php:39 whatsapp.php:32
msgid "Settings"
msgstr ""

#: includes/nta-whatsapp-shortcode.php:136
#: includes/nta-whatsapp-shortcode.php:163
msgid "Online"
msgstr ""

#: includes/nta-whatsapp-shortcode.php:148
#: includes/nta-whatsapp-shortcode.php:176
msgid "Offline"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:6
msgid "Definir texto e estilo para o WhatsApp Flutuante."
msgstr ""

#: views/nta-whatsapp-display-Settings.php:9
msgid "display no desktop"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:18
msgid "display no mobile"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:27
msgid "Texto do Whatsapp Flutuante"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:32
msgid "Chamada do Whatsapp Flutuante"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:37
msgid "Response Time Text"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:42
msgid "Texto do Whatsapp Flutuante Color"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:47
msgid "Cor do Background do Whatsapp Flutuante"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:52
msgid "Posição do Whatsapp Flutuante"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:57
msgid "Left"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:60
msgid "Right"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:68
msgid "Descrição"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:82
msgid "Aviso GDPR"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:97
msgid "display"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:101
msgid "display all pages but except"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:102
msgid "display for pages..."
msgstr ""

#: views/nta-whatsapp-display-Settings.php:109
msgid "Select pages"
msgstr ""

#: views/nta-whatsapp-display-Settings.php:166
msgid "Save Convigurações da Caixa"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:6
msgid "Número da conta ou URL do chat em grupo"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:27
msgid "Texto predefinido"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:38
msgid "Button Label"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:43
msgid ""
"This text applies only on shortcode button. Leave empty to use the default "
"label."
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:49
msgid "Always available online"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:61
msgid "Custom Availability"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:69
msgid "Domingo"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:78
msgid "Aplicar para todos os dias"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:84
msgid "Segunda"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:98
msgid "Terça"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:111
msgid "Quarta"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:126
msgid "Quinta"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:141
msgid "Sexta"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:156
msgid "Sábado"
msgstr ""

#: views/nta-whatsapp-meta-accounts.php:173
msgid "Descrição do texto quando estiver offline"
msgstr ""

#: views/nta-whatsapp-meta-button-style.php:2
msgid ""
"This styling applies only to the shortcode buttons for this account. Leave "
"blank to use the <a href=\"admin.php?page=nta_whatsapp\">default styles set "
"on the Configuraçõespage"
msgstr ""

#: views/nta-whatsapp-meta-button-style.php:5
#: views/nta-whatsapp-Settings.php:15
msgid "Design do Botão"
msgstr ""

#: views/nta-whatsapp-meta-button-style.php:10
#: views/nta-whatsapp-Settings.php:20
msgid "Round"
msgstr ""

#: views/nta-whatsapp-meta-button-style.php:13
#: views/nta-whatsapp-Settings.php:23
msgid "Square"
msgstr ""

#: views/nta-whatsapp-meta-button-style.php:22
#: views/nta-whatsapp-Settings.php:32
msgid "Cor do Background do Botão"
msgstr ""

#: views/nta-whatsapp-meta-button-style.php:29
#: views/nta-whatsapp-Settings.php:39
msgid "Cor do Texto do Botão"
msgstr ""

#: views/nta-whatsapp-selected-accounts.php:6
#: views/nta-whatsapp-woocommerce-button.php:35
msgid "Selecionar todas as Contas:"
msgstr ""

#: views/nta-whatsapp-selected-accounts.php:27
msgid "Remove"
msgstr ""

#: views/nta-whatsapp-Settings.php:5
msgid ""
"Use this form to set default style for shortcode buttons. You can reset the "
"style for individual button when creating/editing a WhatsApp account."
msgstr ""

#: views/nta-whatsapp-Settings.php:8
msgid "Texto do Botão"
msgstr ""

#: views/nta-whatsapp-Settings.php:46
msgid "Save Changes"
msgstr ""

#: views/nta-whatsapp-woocommerce-button.php:8
msgid "Abilitar"
msgstr ""

#: views/nta-whatsapp-woocommerce-button.php:17
msgid "Posição do Botão"
msgstr ""

#: views/nta-whatsapp-woocommerce-button.php:20
msgid "Antes do botão Adicionar ao carrinho"
msgstr ""

#: views/nta-whatsapp-woocommerce-button.php:21
msgid "Após o botão Adicionar ao carrinho"
msgstr ""

#: views/nta-whatsapp-woocommerce-button.php:22
msgid "After short description"
msgstr ""

#: views/nta-whatsapp-woocommerce-button.php:23
msgid "Depois da descrição"
msgstr ""

#: views/nta-whatsapp-woocommerce-button.php:28
msgid "Selecionar contas"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "WhatsApp Chat by NinjaTeam"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://ciawebsites.com.br/wordpress-whatsapp-chat"
msgstr ""

#. Descrição of the plugin/theme
msgid ""
"Integrate your WhatsApp experience directly into your website. This is one "
"of the best way to connect and interact with your customer."
msgstr ""

#. Author of the plugin/theme
msgid "NinjaTeam"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://ciawebsites.com.br"
msgstr ""
